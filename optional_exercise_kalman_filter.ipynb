{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "accurate-religion",
   "metadata": {},
   "source": [
    "## Optional advanced exercise: Kalman filter for track fitting of LHCb data\n",
    "\n",
    "We provide here an additional exercise that you are welcome to try in case you finish the other ones very quickly or if you already had basic knowledge of CUDA programming and wanted to skip the first very basic exercises.\n",
    "\n",
    "It consists of fitting tracks in the VELO tracking detector of the LHCb experiment with a Kalman filter - so it is a more practical HEP example. There is no magnetic field in the VELO detector, so particle trajectories are straight lines. The VELO is a silicon pixel detector providing 3D measurement points in the global coordinate system of LHCb.\n",
    "\n",
    "In this exercise we use CMake for compilation and build libraries from CUDA code. So if you are interested in some of the implementation specifics, feel free to look at the source code for inspiration. A Kalman filter is one method for track fitting, i.e. it happens after the pattern recognition step. This means that the hits originating from the same particle have already been identified and we now want to describe the trajectory such that we can extrapolate for example to other parts of the detector. The Kalman filter subsequently iterates over all hits of a track. For every hit, it estimates the state (i.e. direction and uncertainty) of the track at that point. With the addition of every hit the estimate becomes more precise until the the best linear estimator is achieved at the last hit. \n",
    "\n",
    "For this exercise, we provide you with collection of hits making up the tracks. They are stored in two containers: `hits` contains all space points present on all tracks. `tracks` contains indices to the hits of particular tracks. We also provide the code that runs the Kalman filter on both the CPU and the GPU. The GPU code is however not yet parallelized and no optimizations have been performed. \n",
    "\n",
    "Your task is to parallelize the GPU function and then to optimize step by step the performance.\n",
    "\n",
    "Open the files referenced in every part of the exercise in an editor tab by clicking on them in your clone of the exercise repository!\n",
    "\n",
    "### 1. Compile code\n",
    "Execute the following cells to import some tools and set a few environment variables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "suspended-education",
   "metadata": {},
   "outputs": [],
   "source": [
    "import requests\n",
    "import shutil\n",
    "import os\n",
    "import sys\n",
    "import importlib"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "wicked-passage",
   "metadata": {},
   "outputs": [],
   "source": [
    "pwd = os.path.realpath('.')\n",
    "install_prefix = os.path.join(pwd, 'kalman_filter')\n",
    "data_directory = os.path.join(install_prefix, 'data')\n",
    "binary_prefix = os.path.join(install_prefix, 'kalman_binary')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "million-practice",
   "metadata": {},
   "outputs": [],
   "source": [
    "def build_binary():\n",
    "    pwd = os.path.realpath('.')\n",
    "    build_dir = os.path.join(install_prefix, 'build')\n",
    "    !mkdir -p $build_dir\n",
    "    os.chdir(build_dir)\n",
    "    !cmake -Wno-dev -DCMAKE_INSTALL_PREFIX=$binary_prefix ..\n",
    "    !make install\n",
    "    os.chdir(pwd)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "neutral-lyric",
   "metadata": {},
   "outputs": [],
   "source": [
    "build_binary()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "spanish-beatles",
   "metadata": {},
   "source": [
    "### 2. Download input data\n",
    "\n",
    "Execute the following cells to download the input data (1000 simulated events), i.e. the `hits` and `tracks` directories. They each contain one file per proton-proton collision event with the information about the hits (i.e. 3D space points) and the tracks (i.e. which hits make up the track). Note that unpacking the hits and tracks data will take a little while.\n",
    "\n",
    "The hits and tracks in the files will be read by the program and stored in the following way:\n",
    "This hits are stored in AoS containers as shown in the picture. The offsets to the first hit of every event are also stored.\n",
    "<img src=\"kalman_filter/figures/hits_SoA.png\">\n",
    "The hit structure is defined like this. For our purpose, only the x, y and z position are of interest.\n",
    "<img src=\"kalman_filter/figures/hit_struct.png\">\n",
    "The tracks are stored in structs specifying the number of hits on the track and the indices to the hits of this trak within the hits array\n",
    "<img src=\"kalman_filter/figures/track_struct.png\">\n",
    "The output of the Kalman filter will be the track state closest to the beam line. It is stored in a structure specifying the x, y, z position closest to the beam line as well as the slopes tx = dx/dz and ty = dy/dz of the straight line and the covariance elements computed with the Kalman filter (c00, c11, etc.).\n",
    "<img src=\"kalman_filter/figures/state_struct.png\">\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "narrow-african",
   "metadata": {},
   "outputs": [],
   "source": [
    "def download_file(url, local_filename=None):\n",
    "    if local_filename is None:\n",
    "        local_filename = url.split('/')[-1]\n",
    "    with requests.get(url, stream=True) as r:\n",
    "        with open(local_filename, 'wb') as f:\n",
    "            shutil.copyfileobj(r.raw, f)\n",
    "\n",
    "    return local_filename"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "sealed-driver",
   "metadata": {},
   "outputs": [],
   "source": [
    "def download_data():\n",
    "    pwd = os.path.realpath('.')\n",
    "    !mkdir -p $data_directory\n",
    "    os.chdir(data_directory)\n",
    "    if not os.path.exists(os.path.join(data_directory, 'hits.zip')):\n",
    "        !wget \"https://cernbox.cern.ch/index.php/s/2Rn3m7ESbKaZFAa/download\" -O hits.tar.gz\n",
    "        !wget \"https://cernbox.cern.ch/index.php/s/lXm6eKC5Un8PgUz/download\" -O tracks.tar.gz\n",
    "        !tar zxf hits.tar.gz\n",
    "        !tar zxf tracks.tar.gz\n",
    "    os.chdir(pwd)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "documented-heating",
   "metadata": {},
   "outputs": [],
   "source": [
    "download_data()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "compressed-submission",
   "metadata": {},
   "source": [
    "### 3. Run and understand sequential implementation\n",
    "\n",
    "\n",
    "\n",
    "Start by taking a look at the initial implementation located in the `start` directory. The main executable is defined in `src/kalman_filter.cu`. It takes care of memory (de-)allocations, memory transfers and calls the CPU and GPU functions. The time of the GPU execution is measured using the `std::chrono` library and the results of the computations on the CPU and the GPU are compared in the end. \n",
    "The CPU function to call the Kalman filter for all events is also defined in `src/kalman_filter.cu`. The gpu function (`__global__`) and the combined `__host__ __device__` function to perform the actual fit are defined in `src/kalman_filter_impl.cu`. The definitions of Hit, Track ect. are defined in definitions.cuh and functions needed to read in the hits and tracks are in utils.cu.\n",
    "\n",
    "The binary we compiled before is configued to take five input arguments: \n",
    "    \n",
    "    part (string): one of [\"start\", \"pinned_host_memory\", \"streams\"]\n",
    "    number of events (int)\n",
    "    data location (string): 'kalman_filter/data/'\n",
    "    number of repetitions\n",
    "    device ID (int): leave at 0\n",
    "    number of CUDA streams: only has an effect in the \"streams\" part of the exercise\n",
    "\n",
    "Choose one of `start`, `pinned_host_memory` or `streams` depending on which part of the exercise you are working on. Note that whenever you make changes in the source code you have to re-run the `build_binary()` command. \n",
    "\n",
    "Let's start by executing the initial version of the code located in the `start` directory:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "speaking-fluid",
   "metadata": {},
   "outputs": [],
   "source": [
    "!./run-exclusive.sh $binary_prefix/bin/KalmanFilter start 10 $data_directory 10 0 0"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fossil-lecture",
   "metadata": {},
   "source": [
    "This executed the Kalman filter application defined in `start` over 10 events, using as input the data in the `$data_directory`, looping over the same 10 events 10 times (number of repetitions), using device 0 and using one CUDA stream. Note that the number of CUDA streams will only be relevant for a later part of the exercise. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "crucial-franchise",
   "metadata": {},
   "source": [
    "### 4. Run the Kalman filter on the GPU in parallel\n",
    "\n",
    "The current code in the `start` directory executes the loop over all events and over every track in one event sequentially on the GPU. Think about how to parallelize the problem and run the Kalman filter on the GPU in parallel. For this, you should modify `start/src/kalman_filter.cu` and `start/src/kalman_filter_impl.cu`.\n",
    "\n",
    "Test different numbers of threads per block and blocks per grid and study the impact on the speed of your code. Run many repetitions (hundreds), that will make the time measurement more reliable. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "rising-creator",
   "metadata": {},
   "outputs": [],
   "source": [
    "build_binary()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "direct-crack",
   "metadata": {},
   "outputs": [],
   "source": [
    "!./run-exclusive.sh $binary_prefix/bin/KalmanFilter start 10 $data_directory 10 0 0"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "monthly-vacation",
   "metadata": {},
   "source": [
    "### 5. Double versus single precision\n",
    "\n",
    "In `start/include/definitions.cuh`, a switch between single and double precision is implemented. It works by using a global typedef: `typedef float my_float_t` or `typedef double my_float_t`. All floating point variables necessary for the Kalman filter are implemented as `my_float_t`. By changing the definition from double to float you can test the difference. Do you observe a difference in execution speed and accuracy? Think about which types of calculations can require double precision rather than single precision."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8f25cdf8",
   "metadata": {},
   "outputs": [],
   "source": [
    "build_binary()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "81b54bb1",
   "metadata": {},
   "outputs": [],
   "source": [
    "!./run-exclusive.sh $binary_prefix/bin/KalmanFilter start 10 $data_directory 10 0 0"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "moral-standard",
   "metadata": {},
   "source": [
    "### 6. Multiple streams\n",
    "\n",
    "For this exercise, start from the code located in the directory `streams`. The code structure is similar as in the `start` directory.\n",
    "\n",
    "Use several CUDA streams to hide the latency of copying data to the device now. For simplicity, we copy the same input data to the device for every stream. So basically the same calculation is repeated several times. In an actual setup, one would run over different data sets. There is already one device array per stream and one output states array per stream defined in the code. Include all the code that is relevant for stream processing, marked by `to do`!\n",
    "\n",
    "Remember that a CUDA stream is created with \n",
    "```\n",
    "cudaStream_t stream;\n",
    "cudaStreamCreate(&stream);\n",
    "```\n",
    "and passed to the kernel as fourth argument after the grid and block dimensions and optionally the size of shared memory like so:\n",
    "```\n",
    "my_kernel<<<10,10,0,stream>>>(argumets)\n",
    "```\n",
    "Memory copies on specific streams are similar to the `cudaMemcpy` syntax:\n",
    "```\n",
    "cudaMemcpyAsync( a_d, &a_h, size, cudaMemcpyHostToDevice, stream);\n",
    "```\n",
    "\n",
    "A stream is destroyed with \n",
    "```\n",
    "cudaStreamDestroy(stream);\n",
    "```\n",
    "Note that there is a specific synchronization function that blocks until all work on one specific stream has finished. This means one does not have to use `cudaDeviceSynchronize()`, which would wait for all streams to finish (all GPU work to be done), but rather:\n",
    "```\n",
    "cudaStreamSynchronize(stream[i_stream]);\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "running-calcium",
   "metadata": {},
   "outputs": [],
   "source": [
    "build_binary()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "handmade-documentation",
   "metadata": {},
   "outputs": [],
   "source": [
    "!./run-exclusive.sh $binary_prefix/bin/KalmanFilter streams 10 $data_directory 10 0 0"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "analyzed-mortgage",
   "metadata": {},
   "source": [
    "What is your observation with respect to speed? "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "killing-charlotte",
   "metadata": {},
   "source": [
    "### 7. Pinned host memory\n",
    "\n",
    "Memory copies to the GPU are executed from page-locked memory via direct memory access (DMA). Memory allocated with `malloc` or `new` is not by default page-locked memory, but instead could be swapped out to an external disk for example. When copying data from the host to the GPU, it is therefore first copied to page-locked memory and only then to the GPU. This means that two copies are actually taking place. \n",
    "\n",
    "In CUDA, we can alloated directly page-locked host memory to store host variables. This then avoids the additional copy. However, memory allocated like this cannot be infinitely large. The syntax is as follows:\n",
    "```\n",
    "cudaMallocHost( (void**) &h_a, size_in_bytes )\n",
    "cudaFreeHost( h_a )\n",
    "```\n",
    "\n",
    "Think about which memory arrays would make sense to store in pinned host memory rather than host memory allocated with `new`. Start from the code in the directory `pinned_host_memory` and implement the memory allocation of the host arrays with pinned memory intsead of `new`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "polar-comedy",
   "metadata": {},
   "outputs": [],
   "source": [
    "build_binary()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "exact-buying",
   "metadata": {},
   "outputs": [],
   "source": [
    "!./run-exclusive.sh $binary_prefix/bin/KalmanFilter pinned_host_memory 10 $data_directory 10 0 0"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "automated-enemy",
   "metadata": {},
   "source": [
    "What do you infer from the speed difference between this last implementation and the previous ones? What does this mean for executing this particular code on the GPU? Does it make sense on its own?"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Raw Cell Format",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
